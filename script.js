var points = [
    { label: 'A', x:250 , y: 0 },
    { label: 'B', x:0 , y: 250 },
    { label: 'C', x:500 , y: 78 },
    { label: 'D', x:0 , y: 78 },
    { label: 'E', x:500 , y: 250 }
]

Vue.component('my-polygon', {
    props: ['points'],
    template: '#star-template', 
    computed: {
        pointToString() {
            var points = this.points.map((value) => {
                return [value.x, value.y].join(',')
            });
            return points.join(' ');
        }
    },
    components: {
        'point-label': {
            props: ['point'],
            template: '#label-template'
        }
    }
})

new Vue({
    el: '#main',
    data: {
        points: points,
        isToastVisible: false,
        newPoint: {
            label: '',
            x: 0,
            y: 0
        }
    },
    methods: {
        remove: function(point) {
            if (points.length > 3) {
                points.splice(points.indexOf(point), 1);
            } else {
                this.isToastVisible = true;
                $('.toast').toast({delay: 2000});
                $('.toast').toast('show')
            }
        },
        add: function(e) {
            e.preventDefault()
            points.push({
                label: this.newPoint.label,
                x: this.newPoint.x,
                y: this.newPoint.y
            });
            this.newPoint.label = '';
            this.newPoint.x = 0;
            this.newPoint.y = 0;
        }
    }
})

