# polygon-generator

Generador de polígonos con Vue JS

## Conceptos a desarrollar

Vue, components, props, template, computed, sub components, methods

## Tecnologías

Lenguaje: Javascript

Framework: Vue JS

Gestor de paquetes de javascript: npm

CLI: Vue CLI (está incluida en Vue)

## Uso
Para ejecutar la aplicación ejecutar los siguientes comandos:

```bash
1) npm install
2) npm run serve
```

## Referencias
[vuejs.org](https://es.vuejs.org/v2/examples/svg.html)
